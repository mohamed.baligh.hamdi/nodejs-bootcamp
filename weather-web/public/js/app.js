// this is the client side javascript
console.log("Hello from client side javascript!")

// Please note the use of fetch function in the client side javascript
// client-side code need to be plain javascript, no nodejs is called up here
// nodejs is only server side

const weatherForm = document.querySelector('form')
const cityInput = document.querySelector('input')
const messageOne = document.querySelector('#message-1')
const messageTwo = document.querySelector('#message-2')

weatherForm.addEventListener('submit', (e) => {
    // preventing page refresh on client side
    e.preventDefault()
    // get value from city input
    const location = cityInput.value
    fetch("http://localhost:3001/weather?address="+location).then((response) => {
    response.json().then((data) => {
        if (data.error)
            messageTwo.textContent = data.error
        else
        {
            messageOne.textContent = "Current temperature in "+location+" is :"
            messageTwo.textContent = data.weather.temperature
        }
    })
})
})
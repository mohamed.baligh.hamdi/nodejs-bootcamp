// server side javascript

const express = require('express')
const path = require("path")
const app = express()
const hbs = require('hbs')

// read web server port from command line
const defaultPort = 3000
let port = process.argv[2]
console.log(port)
if (port === undefined)
    port = defaultPort
console.log(port)

// // configuring html public folder
const publicPath = path.join(__dirname, "../public" )
app.use(express.static(publicPath))

// configuring express to use a view engine
// in our case, that will be HBS that is autmatically pluged with express
app.set('view engine','hbs')

// configure view templates folder
const templatesFolderPath = path.join(__dirname, "../templates/views")
app.set('views', templatesFolderPath)

// register partial views
const partialsPath = path.join(__dirname, "../templates/partials")
hbs.registerPartials(partialsPath)



// configuring app rendering

app.get('', (req, res)=> {
    // here we're telling express to use the index.hbs file in the views folder that we set above using the set method
    res.render('index', {
        body: 'Welcome to Weather Forecast App'
    })
})

app.get('/about', (req, res)=> {
    // here we're telling express to use the about.hbs file in the views folder that we set above using the set method
    res.render('about', {
        message: 'Created by',
        author: 'Baligh'
    })
})

app.get('/help', (req, res) => {
    res.render('help', {
        message: 'Help page'
    })
})

app.get('/about', (req, res)=> {
    // here we're telling express to use the index.hbs file in the views folder
    res.render('about', {
        message: 'created by Baligh'
    })
})

// app.get('/weather', (req, resp) => {
//     if (req.query.address == undefined || req.query.address == '')
//         resp.render("404", {
//             message: 'Please specify a city in search query.'
//         })
//     else
//     {
//         request.get('http://localhost:3001/weather?address='+req.query.address, (error, response)  => {
//             if (error)
//                 resp.render("404", {
//                     message: 'Please specify a city in search query.'
//                 }) 
//             else
//             {
//                 const bodyObject = JSON.parse(response.body)
//                 if (bodyObject.error)
//                 {
//                     debugger
//                     resp.render("404", {
//                     message: 'Please specify a city in search query.'
//                     }) 
//                 }
//                 else
//                 {
//                     debugger
                    
//                     resp.render("weather", {
//                         city: req.query.address,
//                         temperature: bodyObject.weather.temperature
//                     })
//                 }
//             }
//         })
//     }

// })

// configure not found page
app.get('*', (req, resp) => {
    resp.render('404', {
        message: 'route not found'
    })
})

app.listen(port, ()=> {
    console.log('listening to port '+port)
})
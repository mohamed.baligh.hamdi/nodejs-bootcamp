This project is developed as part of the following udemy course: https://www.udemy.com/the-complete-nodejs-developer-course-2/  
This code represents my first confrontation to nodejs.  
I probably should work on my best practises when it comes with node/javascript development. So, I will be very glad to have your opinions on my code as well as the projects architecure.

Let me start by walking you thourgh the repository.

# Description

This GIT repository includes 3 projects that helped me make a better understanding of nodejs, express, javascript and ES6 standards.

# Notes-app

Notes-app is the first app that I developed as part of this course.
This is is a nodejs console application that manages write/read operations on file system.
The application can accept 4 commands:
1. **add**: adds a note into a notes.json file located in notes-app project root. The add command accepts mandatory inputs: *--title* which represents the node title and *--body* which represents the note body.
For example, the following command:
`node app.js add --title='Shopping' --body="It's sunday, shopping day"`
will add the following line in *notes.json* file:
`[{"title":"Shopping","body":"It's sunday, shopping day"}]`
2. **remove**: works just like the add command. The only difference is that it just needs to *--title* option.
3. **list**: accepts no arguments and prints all notes into node's console.
4. **read**: reads a specific note, the one specified in the title option.

For developement purpose, I used *yargs* as an npm library for managing complex node commands.

# Weather Forecast Application: *weather-api* & *weather-web*

These two projects combined represent the Weather Forecast Application.

## weather-api: represents the backend service. 
It is written using javascript and served using express.
The app combines calls to https://api.mapbox.com and https://api.darksky.net in order to return the weather forecast given an address as an input.
In order to host the API, you can point to the project root and run something like the following:

`node app.js 3001`

*3001* is nothing but the port against which the API will be hosted

The api can also be hosted using the following npm command:

`npm run start`

This command is configured on *package.json* *scripts* section and will host the api on its default port (3001)

## weather-web: repsents the client side app.  It is also served using express. The view engine that used for templatization is handlebards.  
The weather-web app points to the weather-app back-service and passes the address which is provided by the user on client side.  
The weather-web displays the returned temperature on client side using javascript (client side javascript this time).  

In order to host the web site, you can point on the project root and run something like the following:

`node ./src/server.js 3000`  

*3000* is the hosting port

The web site can also be hosted using the following npm command:

`npm run start`

This command is configured on *package.json* *scripts* section and will run the web site on its default port (3000)

In order to make the two projects work together, you should host them seperatly, on different ports of course (sorry if I insulted your intelligence but I like insisting on details even though they are small :))  

# Playground:

This is where I am just playing with javascript and node, nothing serious is implemented in this folder.

*Please note that, in order for the project to work, please install all dependencies that are informed in package.json file.*

Patiently waiting for your reviews !
const request =  require("./node_modules/request")

const forecast = ({longitude, latitude}, callback) => { 
    const url = "https://api.darksky.net/forecast/cb497d9af8fd540bc7a454e547f9f760/"+longitude+","+latitude+"?units=si"
    debugger
    request.get({url, json: true}, (error, {body}) => {
    if (error)
        callback("Unable to reach weather forecast service !")
    else if (body.error)
        callback("Error while processing weather forecast service")
    else
        callback(undefined, body.currently)
})
}

module.exports = forecast
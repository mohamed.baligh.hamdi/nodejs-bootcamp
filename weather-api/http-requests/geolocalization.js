const request =  require("./node_modules/request")

const geo = (address, callback) => {
    const url = "https://api.mapbox.com/geocoding/v5/mapbox.places/"+encodeURIComponent(address)+".json?access_token=pk.eyJ1IjoiYmhhbWRpIiwiYSI6ImNqdzQ0NndxMjAwYXo0OXBiZW8zYmYzMTEifQ.b8A1Ule5VpyxFe2fGE-JTw"
    request.get({url, json: true}, (error, {body}) => {
    if (error)
        callback("Unable to reach geo-localisation service !")
    else if (body.error)
        callback("Error while processing geo-localisation service")
    else if (body.features.length === 0)
        callback("Please check the given address")
    else
    {
        callback(undefined, {
            longitude: body.features[0].center[1],
            latitude: body.features[0].center[0]
        })
    }
    })
}

module.exports = geo
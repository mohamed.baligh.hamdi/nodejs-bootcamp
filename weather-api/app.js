const geo = require('./http-requests/geolocalization')
const forecast = require('./http-requests/weatherforecast')
const chalk = require('chalk')
const express = require('express')
const cors  = require('cors')

const app = express()

let port = process.argv[2]
const defaultPort = 3001
if (port === undefined)
    port = defaultPort

// enable cors for all requests
app.use(cors())

app.get('/weather', (req, resp)=> {
    const city = req.query.address
    if (city == undefined)
        resp.send({error: "Please specify an address", data:undefined})
    else
    {
        geo(city, (error, data) => {
            if (error)
                resp.send({error: error, data:undefined})
            else{
                forecast(data, (error, data)=> {
                    if (error)
                        resp.send({error: error, data:undefined})
                    else
                    {
                        console.log(chalk.inverse.blue("Current tempertaure for "+city+" is the following: "+data.temperature))
                        resp.send({weather : data, error: undefined})
                    }
                })
            }
        })
    }
})
// }
app.listen(port, () => {
    console.log('Weather API listeneing on port '+port)
})
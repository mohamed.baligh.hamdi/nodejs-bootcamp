const yargs = require("yargs")
const dataManager = require('./data/notes-data-manager')

// creating title builder option (common for all args commands)
const title = {
    description : 'note title',
    demandOption: true,
    type: 'string'
}

// configuring add command
yargs.command({
    command : 'add',
    describe : 'Add new command',
    builder: { 
        title,
        body: {
            description: 'note body',
            demandOption: true,
            type : 'string'
        }
    },
    handler(argv) {
        const note = {
            title : argv.title,
            body : argv.body
        }
        dataManager.addNote(note)
    }
})

// configuring remove command

yargs.command({
    command : 'remove',
    describe : 'Remove a note', 
    builder : {
        title
    },
    handler(argv) {
        dataManager.removeNotes(argv.title)
  }
})

// configuring list command

yargs.command({
    command : 'list',
    describe : 'List of notes',
    handler() {
        dataManager.printAllNotes()
    }
})

// configuring read command

yargs.command({
    command : 'read',
    describe : 'read command',
    builder : {
        title
    },
    handler : function(yargv) {
        dataManager.printNote(yargv.title)
    }
})

module.exports = yargs

const fileName = 'notes.json'
const fs = require('fs')
const chalk = require('chalk')

const addNote = (note) =>
{
    const existingNotes = loadNotes()
    // 1 - check if note already exist
    const noteExists = findNote(existingNotes, note.title)
    // 2 - add note to file if it doesn't exist / print an error in console if it does exist
    if (!noteExists)
    {
        existingNotes.push(note)
        saveNotes(existingNotes)
        console.log(chalk.inverse.green("new note "+note.title+" added"))
    }
    else
        console.log(chalk.inverse.red("note "+note.title+ " already exists"))
}
const loadNotes = () =>
{
    try
    {
        const jsonNotes = fs.readFileSync(fileName)
        return JSON.parse(jsonNotes.toString())
    }
    catch(e)
    {
        console.warn("File "+fileName+" does not exist")
        return []
    }
}

const removeNotes = (noteTitle) => {
    const notes = loadNotes()
    const note = findNote(notes, noteTitle)
    if (note)
    {
        // filter the existing notes based on the note that we would like to remove
        const newNotes = notes.filter((n) => n.title != noteTitle)
        // persist the result in file
        saveNotes(newNotes)
        // log success message in green
        console.log(chalk.inverse.green("note "+noteTitle+" successfully removed from"+fileName+" file"))
    }
    else
        console.log(chalk.inverse.red("Note "+noteTitle+" does not exists in file"))
}

const printAllNotes = () => {
    const notes = loadNotes()
    console.log(chalk.blue.bold("List of notes"))
    notes.forEach((node)=> console.log(node.title))
}

const printNote = (title) => {
    const notes = loadNotes()
    const note = findNote(notes, title)
    debugger
    if (!note)
        console.log(chalk.red("note "+title+" is not found"))
    else
    {
        // const note = notes.filter((n)=>n.title === title)[0]
        console.log(chalk.bold.blue(note.title))
        console.log(note.body)
    }
}

const saveNotes = (notes) =>
{
    fs.writeFileSync(fileName, JSON.stringify(notes))
}
const findNote = (notes, title) =>
{
    return notes.find((note) => note.title === title)
} 
module.exports = {
    addNote : addNote,
    removeNotes: removeNotes,
    printAllNotes: printAllNotes,
    printNote : printNote
}
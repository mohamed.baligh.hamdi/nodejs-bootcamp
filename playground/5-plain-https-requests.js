const https = require('https')
const url = "https://api.darksky.net/forecast/cb497d9af8fd540bc7a454e547f9f760/45,20?units=si"
const request = https.request(url, (response) => {
    let data
    response.on('data', (chunck) => {
        data = data + chunck.toString()
    } )

    response.on('end', () => {
        console.log(JSON.parse(data))
    })
})

request.on('error', (error) => {
    console.log(error)
})

request.end()
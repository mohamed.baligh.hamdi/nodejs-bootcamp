// object property shorthand
const name = 'Baligh'
const age = 28
const employee = {
    name,
    age
}
console.log(employee.name, employee.age)

// object destructuring
const {name:employeeName, age:employeeAge} = employee
console.log(employeeName, employeeAge)
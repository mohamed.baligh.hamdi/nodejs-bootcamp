console.log("Starting")
const timeout = 2000

setTimeout(() => {
    console.log("2 seconds passed.")
}, timeout)

setTimeout(() => {
    console.log("function ran right away")
}, 0)

console.log("Ending")

// console will display:

// Starting
// Ending
// function ran right away.
// 2 seconds passed.

// note that the 0 seconds callback will be called only when the main thread is finished
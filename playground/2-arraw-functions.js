const square = (x) => x*x

console.log(square(9))

const event = {
    name: 'Birthday Party',
    guests: ['Mike','John','Lisa'],
    // we cannot use arrow functions in this context, because we will not be allowed to access name property
    printEvent() {
        console.log("My Name is" +this.name)
        this.guests.forEach((guest)=>{console.log(guest+" is attending party "+this.name)})
    }
}

event.printEvent()
const greeter = (name) => {
    console.log("Hello "+name)
}

//Hello Baligh
greeter("Baligh")

// No javascript error, but program prints Hello undefined
greeter()

const greeterWithDefaultParam = (name = 'Default user') => {
    console.log("Hello "+name)
}
// prints Hello Default user
greeterWithDefaultParam()

// default params with object destructuring

const transaction = (orderId, {label, price}) =>
{
    console .log("order: "+orderId+" is passed for product"+label+" with price"+price)
}
// we have an execution error saying that we cannot destructure undefined value
// the solution will be to initialize 
// transaction(1)

// we initialized the product param to a default object using the '=' operator on param level
// we can initialize every property seprately using the '=' operator on property level
const transactionWithDefaultProduct = (orderId, {label = 'yeezy shoes', price= 180.0} = {}) =>
{
    console .log("order: "+orderId+" is passed for product "+label+" with price "+price+".")
}
// we have an execution error saying that we cannot destructure undefined value
// the solution will be to initialize 
transactionWithDefaultProduct(1)
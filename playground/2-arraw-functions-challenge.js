const taskList = { tasks :[
        {
          text: 'task 1'  ,
          completed : true
        },
        {
          text: 'task 1'  ,
          completed : false
        },
        {
          text: 'task 1'  ,
          completed : false
        }
    ],
    getNonCompletedTasks () {
        return this.tasks.filter((task) => task.completed == false);
    }
}
const nonCompletedTasks = taskList.getNonCompletedTasks()
console.log(nonCompletedTasks)
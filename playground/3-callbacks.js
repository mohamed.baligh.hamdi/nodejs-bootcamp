// setTimeout(() => {
//     // simple callback
//     console.log("callback")
// }, 20);

// const names = ['Baligh','John','Yike']
// const shortNames = names.filter(//other simple callback function
//     (name)=>name.length <=4)
// console.log(shortNames)

// callbacks are generaly used in order to return data from a nested asynchronous function
// Let's illustrate this with a example


// In this first example, we are illustrating a wrong manner for managing communication between callback and parent function
const geolocationWrong = (address) => {
    setTimeout(() => {
       const data = {
           logitude: 15.5,
           latitude: 40.2
       }
       // returns data object only for the current callback function
       // this will not propagate to the parent function
       return data
    }, 2000);   
}

const data = geolocationWrong("Los Angeles")
// this will display undefined, because the data was displayed before the callback is called
console.log(data)


// The second example, is the good way to manage communications between asynchronous callbacks and client code
// we will introduce a callback as a new parameter in the function definition

const geolocationRight = (address, callback) => {
    setTimeout(() => {
       const data = {
           logitude: 15.5,
           latitude: 40.2
       }
       // we will not directly return the data, but we will pass the data to the callback specified as a param
       callback(data)
    }, 2000);   
}

// now, we will call gelocation with a callback that will execute a code based on the passed data
const geoCall = geolocationRight('address', (data)=> console.log(data))
// the code above will correctly display the data like specified in the callback function body

// training, develop a callback that sums two numbers using the callback pattern

const add = (x1, x2, callback) =>
{
    setTimeout(() => {
        callback(x1+x2)
    }, 2000);
}

add(1,2, (sum)=> console.log(sum))
const fs = require('fs')
const book = {
    title : 'Eye is the energy',
    author : 'Ryan Holiday'
}

// serialize data to string using JSON object
const bookJsonString = JSON.stringify(book)
console.log(bookJsonString)
// deserialize data from string to object using JSON.parse
const parsedBook = JSON.parse(bookJsonString)
console.log(parsedBook.author)
fs.writeFileSync('1-json.json', bookJsonString)
const myNotes = fs.readFileSync('1-json.json')
console.log(myNotes.toString())
const objectData = JSON.parse(myNotes.toString())
console.log(objectData.title)

